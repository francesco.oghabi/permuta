<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Costomkt
 *
 * @ORM\Table(name="costomkt")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CostomktRepository")
 */
class Costomkt
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="idScaglione", type="integer")
     */
    private $idScaglione;

    /**
     * @var int
     *
     * @ORM\Column(name="idRapporto", type="integer")
     */
    private $idRapporto;

    /**
     * @var float
     *
     * @ORM\Column(name="variabilita", type="float")
     */
    private $variabilita;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idScaglione
     *
     * @param integer $idScaglione
     *
     * @return Costomkt
     */
    public function setIdScaglione($idScaglione)
    {
        $this->idScaglione = $idScaglione;

        return $this;
    }

    /**
     * Get idScaglione
     *
     * @return int
     */
    public function getIdScaglione()
    {
        return $this->idScaglione;
    }

    /**
     * Set idRapporto
     *
     * @param integer $idRapporto
     *
     * @return Costomkt
     */
    public function setIdRapporto($idRapporto)
    {
        $this->idRapporto = $idRapporto;

        return $this;
    }

    /**
     * Get idRapporto
     *
     * @return int
     */
    public function getIdRapporto()
    {
        return $this->idRapporto;
    }

    /**
     * Set variabilita
     *
     * @param float $variabilita
     *
     * @return Costomkt
     */
    public function setVariabilita($variabilita)
    {
        $this->variabilita = $variabilita;

        return $this;
    }

    /**
     * Get variabilita
     *
     * @return float
     */
    public function getVariabilita()
    {
        return $this->variabilita;
    }
}
