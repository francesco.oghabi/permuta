<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Prezzi
 *
 * @ORM\Table(name="articoli_prezzi")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PrezziRepository")
 */
class Prezzi
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="articolo", type="string")
     */
    private $articolo;

    /**
     * @var int
     *
     * @ORM\Column(name="listino", type="string")
     */
    private $listino;

    /**
     * @var int
     *
     * @ORM\Column(name="prezzo", type="decimal", length=15)
     */
    private $prezzo;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idProdotto
     *
     * @param integer $idProdotto
     *
     * @return Prezzi
     */
    public function setIdProdotto($idProdotto)
    {
        $this->idProdotto = $idProdotto;

        return $this;
    }

    /**
     * Get idProdotto
     *
     * @return int
     */
    public function getIdProdotto()
    {
        return $this->idProdotto;
    }

    /**
     * Set idCondizione
     *
     * @param integer $idCondizione
     *
     * @return Prezzi
     */
    public function setIdCondizione($idCondizione)
    {
        $this->idCondizione = $idCondizione;

        return $this;
    }

    /**
     * Get idCondizione
     *
     * @return int
     */
    public function getIdCondizione()
    {
        return $this->idCondizione;
    }

    /**
     * Set prezzo
     *
     * @param integer $prezzo
     *
     * @return Prezzi
     */
    public function setPrezzo($prezzo)
    {
        $this->prezzo = $prezzo;

        return $this;
    }

    /**
     * Get prezzo
     *
     * @return int
     */
    public function getPrezzo()
    {
        return $this->prezzo;
    }

    /**
     * Set articolo
     *
     * @param integer $articolo
     *
     * @return Prezzi
     */
    public function setArticolo($articolo)
    {
        $this->articolo = $articolo;

        return $this;
    }

    /**
     * Get articolo
     *
     * @return integer
     */
    public function getArticolo()
    {
        return $this->articolo;
    }

    /**
     * Set listino
     *
     * @param integer $listino
     *
     * @return Prezzi
     */
    public function setListino($listino)
    {
        $this->listino = $listino;

        return $this;
    }

    /**
     * Get listino
     *
     * @return integer
     */
    public function getListino()
    {
        return $this->listino;
    }
}
