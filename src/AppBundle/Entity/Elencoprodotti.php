<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Elencoprodotti
 *
 * @ORM\Table(name="articoli")
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ElencoprodottiRepository")
 */
class Elencoprodotti
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var stringc 
    *
     * @ORM\Column(name="descrizioneProdotto", type="text", nullable=false)
     */
    private $descrizioneprodotto;

    
     /**
     * @var integer 
    *
     * @ORM\Column(name="categoria", type="integer", length=11, nullable = true)
     */
    private $categoria;
    
    /**
     * @var integer 
    *
     * @ORM\Column(name="sottocategoria", type="integer", length=11, nullable = true)
     */
    private $sottocategoria;
    
    /**
     * @var integer 
    *
     * @ORM\Column(name="codice", type="string", length=255, nullable = true)
     */
    //CODICE INVOICEX
    private $codice;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descrizioneprodotto
     *
     * @param string $descrizioneprodotto
     *
     * @return Elencoprodotti
     */
    public function setDescrizioneprodotto($descrizioneprodotto)
    {
        $this->descrizioneprodotto = $descrizioneprodotto;

        return $this;
    }

    /**
     * Get descrizioneprodotto
     *
     * @return string
     */
    public function getDescrizioneprodotto()
    {
        return $this->descrizioneprodotto;
    }

    /**
     * Set categoria
     *
     * @param integer $categoria
     *
     * @return Elencoprodotti
     */
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;

        return $this;
    }

    /**
     * Get categoria
     *
     * @return integer
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * Set sottocategoria
     *
     * @param integer $sottocategoria
     *
     * @return Elencoprodotti
     */
    public function setSottocategoria($sottocategoria)
    {
        $this->sottocategoria = $sottocategoria;

        return $this;
    }

    /**
     * Get sottocategoria
     *
     * @return integer
     */
    public function getSottocategoria()
    {
        return $this->sottocategoria;
    }

    /**
     * Set codice
     *
     * @param string $codice
     *
     * @return Elencoprodotti
     */
    public function setCodice($codice)
    {
        $this->codice = $codice;

        return $this;
    }

    /**
     * Get codice
     *
     * @return string
     */
    public function getCodice()
    {
        return $this->codice;
    }
}
