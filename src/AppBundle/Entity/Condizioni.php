<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Condizioni
 *
 * @ORM\Table(name="condizioni")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CondizioniRepository")
 */
class Condizioni
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string

     * @ORM\Column(name="condizione", type="text", nullable=false)
     */
    private $condizione;
    
     /**
     * @var string
     *
     * @ORM\Column(name="codice", type="text", nullable=false)
     */
    private $codice;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set condizione
     *
     * @param string $condizione
     *
     * @return Condizioni
     */
    public function setCondizione($condizione)
    {
        $this->condizione = $condizione;

        return $this;
    }

    /**
     * Get condizione
     *
     * @return string
     */
    public function getCondizione()
    {
        return $this->condizione;
    }

    /**
     * Set codice
     *
     * @param string $codice
     *
     * @return Condizioni
     */
    public function setCodice($codice)
    {
        $this->codice = $codice;

        return $this;
    }

    /**
     * Get codice
     *
     * @return string
     */
    public function getCodice()
    {
        return $this->codice;
    }
}
