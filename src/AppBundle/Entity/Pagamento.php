<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pagamento
 *
 * @ORM\Table(name="pagamento")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PagamentoRepository")
 */
class Pagamento
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="idRitiro", type="string", length=255, nullable=true)
     */
    private $idRitiro;

    /**
     * @var int
     *
     * @ORM\Column(name="metodo", type="integer")
     */
    private $metodo;

    /**
     * @var string
     *
     * @ORM\Column(name="data", type="string", length=255)
     */
    private $data;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idPermuta
     *
     * @param string $idPermuta
     *
     * @return Pagamento
     */
    public function setIdPermuta($idPermuta)
    {
        $this->idPermuta = $idPermuta;

        return $this;
    }

    /**
     * Get idPermuta
     *
     * @return string
     */
    public function getIdPermuta()
    {
        return $this->idPermuta;
    }

    /**
     * Set metodo
     *
     * @param integer $metodo
     *
     * @return Pagamento
     */
    public function setMetodo($metodo)
    {
        $this->metodo = $metodo;

        return $this;
    }

    /**
     * Get metodo
     *
     * @return int
     */
    public function getMetodo()
    {
        return $this->metodo;
    }

    /**
     * Set data
     *
     * @param string $data
     *
     * @return Pagamento
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set idRitiro
     *
     * @param string $idRitiro
     *
     * @return Pagamento
     */
    public function setIdRitiro($idRitiro)
    {
        $this->idRitiro = $idRitiro;

        return $this;
    }

    /**
     * Get idRitiro
     *
     * @return string
     */
    public function getIdRitiro()
    {
        return $this->idRitiro;
    }
}
