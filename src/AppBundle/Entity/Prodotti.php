<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

use Doctrine\ORM\Mapping as ORM;

/**
 * Prodotti
 *
 * @ORM\Table(name="prodotti")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProdottiRepository")
 */
class Prodotti
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    
    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="Permuta", inversedBy="prodotti" ,cascade="persist")
     * @ORM\JoinColumn(name="idPermuta", referencedColumnName="id")
     */

    private $idPermuta;
    

    
    
    /**
     * @var int
     *
     * @ORM\Column(name="product_id", type="string", nullable  = true)
     */
    private $productId;
    
    /**
     * @var string
     *@Assert\NotBlank()
     * @ORM\Column(name="modello", type="string", length=255, nullable=false)
     */
    private $modello;


    /**
     * @var int
     *
     *  @ORM\ManyToOne(targetEntity="Condizioni",cascade="persist")
     * * @ORM\Column(name="condizione_id", type="integer", length=255,nullable=true)
     * * @ORM\JoinColumn(name="condizione_id", referencedColumnName="id")
     */
    private $condizioneId;
    
    /**
     * @var string
     *
     * @ORM\Column(name="altro", type="string", length=255,nullable=true)
     */
    private $altro;
    
   /**
     * @var float
     *
     * @ORM\Column(name="prezzo", type="float", length=255,nullable=true)
     */
    private $prezzo;


   

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set productId
     *
     * @param integer $productId
     *
     * @return Prodotti
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;

        return $this;
    }

    /**
     * Get productId
     *
     * @return int
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * Set condizioneId
     *
     * @param integer $condizioneId
     *
     * @return Prodotti
     */
    public function setCondizioneId($condizioneId)
    {
        $this->condizioneId = $condizioneId;

        return $this;
    }

    /**
     * Get condizioneId
     *
     * @return int
     */
    public function getCondizioneId()
    {
        return $this->condizioneId;
    }

    

    /**
     * Set modello
     *
     * @param string $modello
     *
     * @return Prodotti
     */
    public function setModello($modello)
    {
        $this->modello = $modello;

        return $this;
    }

    /**
     * Get modello
     *
     * @return string
     */
    public function getModello()
    {
        return $this->modello;
    }

    /**
     * Set altro
     *
     * @param string $altro
     *
     * @return Prodotti
     */
    public function setAltro($altro)
    {
        $this->altro = $altro;

        return $this;
    }

    /**
     * Get altro
     *
     * @return string
     */
    public function getAltro()
    {
        return $this->altro;
    }

    /**
     * Set idPermuta
     *
     * @param integer $idPermuta
     *
     * @return Prodotti
     */
    public function setIdPermuta($idPermuta)
    {
        $this->idPermuta = $idPermuta;

        return $this;
    }

    /**
     * Get idPermuta
     *
     * @return integer
     */
    public function getIdPermuta()
    {
        return $this->idPermuta;
    }

    /**
     * Set prezzo
     *
     * @param float $prezzo
     *
     * @return Prodotti
     */
    public function setPrezzo($prezzo)
    {
        $this->prezzo = $prezzo;

        return $this;
    }

    /**
     * Get prezzo
     *
     * @return float
     */
    public function getPrezzo()
    {
        return $this->prezzo;
    }
}
