<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Ritiri
 *
 * @ORM\Table(name="ritiri")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RitiriRepository")
 */
class Ritiri
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="codbartolini", type="string", length=13, nullable=true)
     */
    private $codbartolini;

    /**
     * @var string
     *
     * @ORM\Column(name="vaorsr", type="string", length=35, nullable=true)
     */
    private $vaorsr;

    /**
     * @var string
     *
     * @ORM\Column(name="codicefiscale", type="string", length=20, nullable=true)
     */
    private $codicefiscale;
    /**
    * @ORM\Column(type="string", length=35, nullable=TRUE)
    * INDIRIZZO
    * 
    */
	protected $idPermuta;

    /**
     * @var string
     *
     * @ORM\Column(name="vaoinr", type="string", length=35, nullable=true)
     */
    private $vaoinr;

    /**
     * @var string
     * @ORM\Column(name="vaocar", type="integer", length=9, nullable=true)
     */
    private $vaocar;
    
    /**
     * @var string
     *
     * @ORM\Column(name="vaoncl", type="integer", length=5, nullable=false)
     */
    private $vaoncl;

    /**
     * @var string
     *
     * @ORM\Column(name="vaolor", type="string", length=35, nullable=true)
     */
    private $vaolor;

    /**
     * @var string
     *
     * @ORM\Column(name="vaoprr", type="string", length=2, nullable=true)
     */
    private $vaoprr;

    
    
    /**
     * @var string
     *
     * @ORM\Column(name="vaorer", type="string", length=35, nullable=true)
     */
    private $vaorer;

    /**
     * @var string
     *
     * @ORM\Column(name="vaoter", type="string", length=16, nullable=true)
     */
    private $vaoter;

    /**
     * @var string
     *
     * @ORM\Column(name="vaodar", type="string", length=10, nullable=true)
     * @Assert\Regex("/^(0[1-9]|[12][0-9]|3[01])[-.](0[1-9]|1[012])[-.](19|20)\d\d$/")
     * 
     * 
     */
    private $vaodar;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="vaoor", type="time", nullable=true)
     */
    private $vaoor;

    /**
     * @var string
     
     * @Assert\Email(
     *     message = "Indirizzo mail non valido",
     *     checkMX = true)
     * @ORM\Column(name="vaoemlr", type="string", length=60, nullable=true)
     */
    private $vaoemlr;

    /**
     * @var string
   
     * @ORM\Column(name="vaosmsr", type="string", length=16, nullable=true)
     */
    private $vaosmsr;

    /**
     * @var bool
     *
     * @ORM\Column(name="gestito", type="boolean")
     */
    private $gestito;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codbartolini
     *
     * @param string $codbartolini
     *
     * @return Ritiri
     */
    public function setCodbartolini($codbartolini)
    {
        $this->codbartolini = $codbartolini;

        return $this;
    }

    /**
     * Get codbartolini
     *
     * @return string
     */
    public function getCodbartolini()
    {
        return $this->codbartolini;
    }

    /**
     * Set vaorsr
     *
     * @param string $vaorsr
     *
     * @return Ritiri
     */
    public function setVaorsr($vaorsr)
    {
        $this->vaorsr = $vaorsr;

        return $this;
    }

    /**
     * Get vaorsr
     *
     * @return string
     */
    public function getVaorsr()
    {
        return $this->vaorsr;
    }

    /**
     * Set codicefiscale
     *
     * @param string $codicefiscale
     *
     * @return Ritiri
     */
    public function setCodicefiscale($codicefiscale)
    {
        $this->codicefiscale = $codicefiscale;

        return $this;
    }

    /**
     * Get codicefiscale
     *
     * @return string
     */
    public function getCodicefiscale()
    {
        return $this->codicefiscale;
    }

    /**
     * Set vaoinr
     *
     * @param string $vaoinr
     *
     * @return Ritiri
     */
    public function setVaoinr($vaoinr)
    {
        $this->vaoinr = $vaoinr;

        return $this;
    }

    /**
     * Get vaoinr
     *
     * @return string
     */
    public function getVaoinr()
    {
        return $this->vaoinr;
    }

    /**
     * Set vaocar
     *
     * @param string $vaocar
     *
     * @return Ritiri
     */
    public function setVaocar($vaocar)
    {
        $this->vaocar = $vaocar;

        return $this;
    }

    /**
     * Get vaocar
     *
     * @return string
     */
    public function getVaocar()
    {
        return $this->vaocar;
    }

    /**
     * Set vaolor
     *
     * @param string $vaolor
     *
     * @return Ritiri
     */
    public function setVaolor($vaolor)
    {
        $this->vaolor = $vaolor;

        return $this;
    }

    /**
     * Get vaolor
     *
     * @return string
     */
    public function getVaolor()
    {
        return $this->vaolor;
    }

    /**
     * Set vaoprr
     *
     * @param string $vaoprr
     *
     * @return Ritiri
     */
    public function setVaoprr($vaoprr)
    {
        $this->vaoprr = $vaoprr;

        return $this;
    }

    /**
     * Get vaoprr
     *
     * @return string
     */
    public function getVaoprr()
    {
        return $this->vaoprr;
    }

    /**
     * Set vaorer
     *
     * @param string $vaorer
     *
     * @return Ritiri
     */
    public function setVaorer($vaorer)
    {
        $this->vaorer = $vaorer;

        return $this;
    }

    /**
     * Get vaorer
     *
     * @return string
     */
    public function getVaorer()
    {
        return $this->vaorer;
    }

    /**
     * Set vaoter
     *
     * @param string $vaoter
     *
     * @return Ritiri
     */
    public function setVaoter($vaoter)
    {
        $this->vaoter = $vaoter;

        return $this;
    }

    /**
     * Get vaoter
     *
     * @return string
     */
    public function getVaoter()
    {
        return $this->vaoter;
    }

    /**
     * Set vaodar
     *
     * @param string $vaodar
     *
     * @return Ritiri
     */
    public function setVaodar($vaodar)
    {
        $this->vaodar = $vaodar;

        return $this;
    }

    /**
     * Get vaodar
     *
     * @return string
     */
    public function getVaodar()
    {
        return $this->vaodar;
    }

    /**
     * Set vaoor
     *
     * @param \DateTime $vaoor
     *
     * @return Ritiri
     */
    public function setVaoor($vaoor)
    {
        $this->vaoor = $vaoor;

        return $this;
    }

    /**
     * Get vaoor
     *
     * @return \DateTime
     */
    public function getVaoor()
    {
        return $this->vaoor;
    }

    /**
     * Set vaomlr
     *
     * @param string $vaoemlr
     *
     * @return Ritiri
     */
    public function setVaoemlr($vaoemlr)
    {
        $this->vaoemlr = $vaoemlr;

        return $this;
    }

    /**
     * Get vaoemlr
     *
     * @return string
     */
    public function getVaoemlr()
    {
        return $this->vaoemlr;
    }

    /**
     * Set vaosmsr
     *
     * @param string $vaosmsr
     *
     * @return Ritiri
     */
    public function setVaosmsr($vaosmsr)
    {
        $this->vaosmsr = $vaosmsr;

        return $this;
    }

    /**
     * Get vaosmsr
     *
     * @return string
     */
    public function getVaosmsr()
    {
        return $this->vaosmsr;
    }

    /**
     * Set gestito
     *
     * @param boolean $gestito
     *
     * @return Ritiri
     */
    public function setGestito($gestito)
    {
        $this->gestito = $gestito;

        return $this;
    }

    /**
     * Get gestito
     *
     * @return bool
     */
    public function getGestito()
    {
        return $this->gestito;
    }

    /**
     * Set idPermuta
     *
     * @param string $idPermuta
     *
     * @return Ritiri
     */
    public function setIdPermuta($idPermuta)
    {
        $this->idPermuta = $idPermuta;

        return $this;
    }

    /**
     * Get idPermuta
     *
     * @return string
     */
    public function getIdPermuta()
    {
        return $this->idPermuta;
    }

    /**
     * Set vaoncl
     *
     * @param integer $vaoncl
     *
     * @return Ritiri
     */
    public function setVaoncl($vaoncl)
    {
        $this->vaoncl = $vaoncl;

        return $this;
    }

    /**
     * Get vaoncl
     *
     * @return integer
     */
    public function getVaoncl()
    {
        return $this->vaoncl;
    }
}
