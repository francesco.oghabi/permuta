<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Permuta
 *
 * @ORM\Table(name="permuta")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PermutaRepository")
 */
class Permuta
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="timestamp", type="datetime", nullable=true)
     */
    private $timestamp = 'now()';
    
    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=255)
     */
    private $nome;
    
    /**
     * @var string
     *
     * @ORM\Column(name="cognome", type="string", length=255)
     */
    private $cognome;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     * @Assert\Email(
     *     message = "Indirizzo mail non valido",
     *     checkMX = true)
     * 
     */
    private $email;
    
    /**
     * @var string
     *
     * @ORM\Column(name="telefono", type="string", length=255)
     */
    private $telefono;
    
     /**
    * @var Prodotti 
    * @ORM\OneToMany(targetEntity="Buy", mappedBy="idPermuta", cascade="persist")
    */
    protected $whatbuy;
    
    /**
     * @var string
     *
     * @ORM\Column(name="checkritiro", type="boolean", length=5)
     */
    private $checkRitiro;
    /**
     * @var string
     *
     * @ORM\Column(name="is_auto", type="boolean", length=5)
     */
    private $is_auto;
    
    
 /**
 * @var Prodotti 
 * @ORM\OneToMany(targetEntity="Prodotti", mappedBy="idPermuta", cascade="persist")
 */
    protected $prodotti;

         public function __construct()
    {
        $this->prodotti = new ArrayCollection();
        $this->whatbuy = new ArrayCollection();
        $this->setTimestamp(new \DateTime());

    }
    
    public function getProdotti()
    {
        return $this->prodotti;
    }
    
    
    public function getWhatbuy()
    {
        return $this->whatbuy;
    }
      
    

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    
    /**
     * Set id
     *
     * @param int $id
     *
     * @return Permuta
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
    
    /**
     * Set nome
     *
     * @param string $nome
     *
     * @return Permuta
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    

    /**
     * Set cognome
     *
     * @param string $cognome
     *
     * @return Permuta
     */
    public function setCognome($cognome)
    {
        $this->cognome = $cognome;

        return $this;
    }

    /**
     * Get cognome
     *
     * @return string
     */
    public function getCognome()
    {
        return $this->cognome;
    }

   

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Permuta
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     *
     * @return Permuta
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }
    
    

    
  

    /**
     * Set timestamp
     *
     * @param \DateTime $timestamp
     *
     * @return Permuta
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;

        return $this;
    }

    /**
     * Get timestamp
     *
     * @return \DateTime
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * Add whatbuy
     *
     * @param \AppBundle\Entity\Buy $whatbuy
     *
     * @return Permuta
     */
    public function addWhatbuy(\AppBundle\Entity\Buy $whatbuy)
    {
        $this->whatbuy[] = $whatbuy;

        return $this;
    }

    /**
     * Remove whatbuy
     *
     * @param \AppBundle\Entity\Buy $whatbuy
     */
    public function removeWhatbuy(\AppBundle\Entity\Buy $whatbuy)
    {
        $this->whatbuy->removeElement($whatbuy);
    }

    /**
     * Add prodotti
     *
     * @param \AppBundle\Entity\Prodotti $prodotti
     *
     * @return Permuta
     */
    public function addProdotti(\AppBundle\Entity\Prodotti $prodotti)
    {
        $this->prodotti[] = $prodotti;

        return $this;
    }

    /**
     * Remove prodotti
     *
     * @param \AppBundle\Entity\Prodotti $prodotti
     */
    public function removeProdotti(\AppBundle\Entity\Prodotti $prodotti)
    {
        $this->prodotti->removeElement($prodotti);
    }

    /**
     * Set checkRitiro
     *
     * @param boolean $checkRitiro
     *
     * @return Permuta
     */
    public function setCheckRitiro($checkRitiro)
    {
        $this->checkRitiro = $checkRitiro;

        return $this;
    }

    /**
     * Get checkRitiro
     *
     * @return boolean
     */
    public function getCheckRitiro()
    {
        return $this->checkRitiro;
    }

    /**
     * Set isAuto
     *
     * @param boolean $isAuto
     *
     * @return Permuta
     */
    public function setIsAuto($isAuto)
    {
        $this->is_auto = $isAuto;

        return $this;
    }

    /**
     * Get isAuto
     *
     * @return boolean
     */
    public function getIsAuto()
    {
        return $this->is_auto;
    }
}
