<?php

namespace AppBundle\Entity;



use Doctrine\ORM\Mapping as ORM;

/**
 * Prodotti
 *
 * @ORM\Table(name="buy")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BuyRepository")
 */
class Buy
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    
    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="Permuta", inversedBy="whatbuy" ,cascade="persist")
     * @ORM\JoinColumn(name="idPermuta", referencedColumnName="id")
     */

    private $idPermuta;
    

    
    
    /**
     * @var int
     *
     * @ORM\Column(name="buy_id_mod", type="string", nullable  = true)
     */
    private $buy_id_mod;
    
    /**
     * @var string
     *
     * @ORM\Column(name="buy_mod", type="string", length=255)
     */
    private $buy_mod;


   


   

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

   

   

    /**
     * Set modello
     *
     * @param string $modello
     *
     * @return Prodotti
     */
    public function setModello($modello)
    {
        $this->modello = $modello;

        return $this;
    }

    /**
     * Get modello
     *
     * @return string
     */
    public function getModello()
    {
        return $this->modello;
    }

    /**
     * Set idPermuta
     *
     * @param integer $idPermuta
     *
     * @return Prodotti
     */
    public function setIdPermuta($idPermuta)
    {
        $this->idPermuta = $idPermuta;

        return $this;
    }

    /**
     * Get idPermuta
     *
     * @return integer
     */
    public function getIdPermuta()
    {
        return $this->idPermuta;
    }

    /**
     * Set buyIdMod
     *
     * @param integer $buyIdMod
     *
     * @return Buy
     */
    public function setBuyIdMod($buyIdMod)
    {
        $this->buy_id_mod = $buyIdMod;

        return $this;
    }

    /**
     * Get buyIdMod
     *
     * @return integer
     */
    public function getBuyIdMod()
    {
        return $this->buy_id_mod;
    }

    /**
     * Set buyMod
     *
     * @param string $buyMod
     *
     * @return Buy
     */
    public function setBuyMod($buyMod)
    {
        $this->buy_mod = $buyMod;

        return $this;
    }

    /**
     * Get buyMod
     *
     * @return string
     */
    public function getBuyMod()
    {
        return $this->buy_mod;
    }
}
