<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Localita
 *
 * @ORM\Table(name="localita")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\LocalitaRepository")
 */
class Localita
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="localita", type="string", length=35, nullable=true)
     */
    private $localita;

    /**
     * @var string
     *
     * @ORM\Column(name="sinonimo", type="string", length=35)
     */
    private $sinonimo;

    /**
     * @var string
     *
     * @ORM\Column(name="cap", type="string", length=5, nullable=true)
     */
    private $cap;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set localita
     *
     * @param string $localita
     *
     * @return Localita
     */
    public function setLocalita($localita)
    {
        $this->localita = $localita;

        return $this;
    }

    /**
     * Get localita
     *
     * @return string
     */
    public function getLocalita()
    {
        return $this->localita;
    }

    /**
     * Set sinonimo
     *
     * @param string $sinonimo
     *
     * @return Localita
     */
    public function setSinonimo($sinonimo)
    {
        $this->sinonimo = $sinonimo;

        return $this;
    }

    /**
     * Get sinonimo
     *
     * @return string
     */
    public function getSinonimo()
    {
        return $this->sinonimo;
    }

    /**
     * Set cap
     *
     * @param string $cap
     *
     * @return Localita
     */
    public function setCap($cap)
    {
        $this->cap = $cap;

        return $this;
    }

    /**
     * Get cap
     *
     * @return string
     */
    public function getCap()
    {
        return $this->cap;
    }
}

