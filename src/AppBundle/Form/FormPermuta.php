<?php

namespace AppBundle\Form;

use Doctrine\ORM\Mapping as ORM;



use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;

use AppBundle\Entity\Permuta;


//PER COLLECTION
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Validator\Constraints as Assert;
 use Symfony\Component\Validator\Constraints\Count;
 
 
 use Symfony\Component\Validator\Constraints\Valid;
class FormPermuta extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nome',TextType::class)
            ->add('cognome',TextType::class)
                ->add('email',TextType::class)
                ->add('telefono',TextType::class)
                ->add('prodotti', CollectionType::class, array(
                      //'error_bubbling' => false,
                'constraints' => array(
                       new Valid(),
                         new Assert\Count([
                             'min' => 1,
                             'minMessage' => 'Inserisci almeno un prodotto da permutare'
                         ]),
                     ),
                    'required' => true,
                    'label' => false,
            'entry_type' => ProdottiType::class,
            'entry_options' => array(
                'label' => false, 'required' => true),
             'allow_add' => true,       
        ))
                
                 ->add('whatbuy', CollectionType::class, array(
                     
                     'label' => false,
            'entry_type' => BuyType::class,
            'entry_options' => array('label' => false),
             'allow_add' => true))       
            ->add('save', SubmitType::class, array(
                 'label' => "Richiedi Valutazione",
                'attr' => array('class' => 'mpb-btn  mpb-btn--blue mpb-btn--64')
            ))
        ;
    }
    
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Permuta::class,
         
        ));
    }
 
    
   
    
}