<?php
namespace AppBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
//use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use AppBundle\Entity\Pagamento;



//PER COLLECTION
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;


class FormPagamento extends AbstractType {
		
	public function buildForm(FormBuilderInterface $builder, array $options) {
    	
		 $builder
                 ->add('idRitiro', HiddenType::class,array('required' => false))
		 ->add('metodo', ChoiceType::class,array('required' => true,'label'=> "Metodo di Pagamento",
				   		'choices'  => array(
        			'Bonifico Bancario' => '1',
				'PayPal'=> '2')))
 
		->add('data', TextType::class,array('required' => false,'label'=> "IBAN o Indirizzo PayPal"));
		 
		   $builder->add('save',  SubmitType::class, array('label' => 'Fatti Pagare',  'attr' => array('class' => 'mpb-btn  mpb-btn--blue mpb-btn--64')));
    }

    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Pagamento::class,
        ));
    }

   
}
