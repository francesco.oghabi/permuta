<?php

namespace AppBundle\Form;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;




use AppBundle\Entity\Prodotti;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProdottiType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('productId',HiddenType::class,array('attr' => array('readonly' => true)))
                 ->add('modello',TextType::class, array('required' => true,'attr' => array('class' => 'form-control','required' => true)))
                ->add('condizioneId',  ChoiceType::class,array(
                    'label' => "Condizione",
                    'attr' => array('class' => 'form-control'),
                    'choices'=> array(
                        'Pari Al Nuovo' =>1,
                        'Ottime' =>2,
                        'Minimi Segni'=>3,
                        'Diversi Segni'=>4,
                        'Molto Usurata'=>5,
                        'Non Funzionante'=>6
                    )))
                ->add('altro',TextType::class, array(
                     'label_attr' => array('id' => 'altro'),
                     'label' => "Aggiungi Altre Informazioni",
                    'required' => false,
                    'attr' => array('class' => 'form-control')));

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Prodotti::class,
        ));
    }
}