<?php

namespace AppBundle\Repository;

/**
 * ElencoprodottiRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ElencoprodottiRepository extends \Doctrine\ORM\EntityRepository
{
    
    
    
    public function findProduct($str){
        return $this->getEntityManager()
            ->createQuery(
               'SELECT e
                FROM AppBundle:Elencoprodotti e
                WHERE e.descrizioneprodotto LIKE :str'
            )
            ->setParameter('str', '%'.$str.'%')
            ->getResult();
    }
    
    
   
    
    
    
}



  
 