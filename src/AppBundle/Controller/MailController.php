<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Permuta;
use AppBundle\Form\FormPermuta;
use AppBundle\Form\ProdottiType;
use AppBundle\Entity\Prodotti;
use AppBundle\Entity\Buy;
use AppBundle\Form\BuyType;
use AppBundle\Entity\Pagamento;
use AppBundle\Form\FormPagamento;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Entity;
use AppBundle\Repository\EntityRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
//JSON
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class MailController extends Controller {


    public function sendmailAction() {



        $name = 'Francesco';
        $message = \Swift_Message::newInstance()
                ->setSubject('Hello Email')
                ->setFrom('info@reflexmania.it')
                ->setTo('info@reflexmania.it')
                ->setBody(
                $this->renderView(
                        // app/Resources/views/Emails/registration.html.twig
                        'Emails/registration.html.twig', array('name' => $name)
                ), 'text/html'
                )
        /*
         * If you also want to include a plaintext version of the message
          ->addPart(
          $this->renderView(
          'Emails/registration.txt.twig',
          array('name' => $name)
          ),
          'text/plain'
          )
         */
        ;

        $this->get('mailer')->send($message);
        // or, you can also fetch the mailer service this way
        // $this->get('mailer')->send($message);
        //return $this->render(...);
    }

    public function mailPermutaAction($generalData, $elenco, $percentuale,$richieste) {

            //MAIL AL CLIENTE
        $name = 'Francesco';
        $message = \Swift_Message::newInstance()
                ->setSubject('RICHIESTA DI PERMUTA AUTOMATICA')
                ->setFrom('info@reflexmania.it')
                ->setTo($generalData[2])
                ->setBody(
                $this->renderView(
                        // app/Resources/views/Emails/registration.html.twig
                        'Emails/autotradein.html.twig', array('generali' => $generalData,
                    'prodotti' => $elenco,
                    'percentuale' => $percentuale,
                            'richieste' => $richieste)
                ), 'text/html'
        );

        $this->get('mailer')->send($message);
        
        
        //MAIL ALL AMMINISTRATORE
        
        $name = 'Francesco';
        $message = \Swift_Message::newInstance()
                ->setSubject('RICHIESTA DI PERMUTA AUTOMATICA')
                ->setFrom('info@reflexmania.it')
                ->setTo('info@reflexmania.it')
                ->setBody(
                $this->renderView(
                        // app/Resources/views/Emails/registration.html.twig
                        'Emails/autotradein.html.twig', array('generali' => $generalData,
                    'prodotti' => $elenco,
                    'percentuale' => $percentuale,
                            'richieste' => $richieste)
                ), 'text/html'
        );

        $this->get('mailer')->send($message);
       
    }

    public function mailadminmanAction($generalData, $elenco) {


        $name = 'Francesco';
        $message = \Swift_Message::newInstance()
                ->setSubject('RICHIESTA DI ACQUISTO DA GESTIRE')
                ->setFrom('info@reflexmania.it')
                ->setTo('info@reflexmania.it')
                ->setBody(
                $this->renderView(
                        // app/Resources/views/Emails/registration.html.twig
                        'Emails/Admin/autotradein.html.twig', array('generali' => $generalData,
                    'prodotti' => $elenco)
                ), 'text/html'
        );

        $this->get('mailer')->send($message);
    }

    
    public function mantradeinAction($generalData, $elenco,$richiesti) {

        //MAIL AL CLIENTE
        $name = 'Francesco';
        $message = \Swift_Message::newInstance()
                ->setSubject('RICHIESTA DI PERMUTA DA GESTIRE')
                ->setFrom('info@reflexmania.it')
                ->setTo('info@reflexmania.it')
                ->setBody(
                $this->renderView(
                        // app/Resources/views/Emails/registration.html.twig
                        'Emails/mantradein.html.twig', array('generali' => $generalData,
                    'prodotti' => $elenco,
                            'richiesti' => $richiesti)
                ), 'text/html'
        );

        $this->get('mailer')->send($message);
        
        //MAIL ALL AMMINISTRATORE
        $name = 'Francesco';
        $message = \Swift_Message::newInstance()
                ->setSubject('RICHIESTA DI PERMUTA DA GESTIRE')
                ->setFrom('info@reflexmania.it')
                ->setTo('info@reflexmania.it')
                ->setBody(
                $this->renderView(
                        // app/Resources/views/Emails/registration.html.twig
                        'Emails/Admin/mantradein.html.twig', array('generali' => $generalData,
                    'prodotti' => $elenco,
                            'richiesti' => $richiesti)
                ), 'text/html'
        );

        $this->get('mailer')->send($message);
        
    }
    
    //MANDO MAIL RITIRO CON ALLEGATO
    public function ritirimailAction($allegato) {
        $oggettoAdmin = "RITIRO";
        //$oggettoUser = "Vendi subito la tua attrezzatura!";

        //MAIL PER CLIENTE
     /*   $message = \Swift_Message::newInstance()
                ->setSubject($oggettoUser)
                ->setFrom('info@reflexmania.it')
                ->setTo($generalData[2])
                ->setBody(
                $this->renderView(
                        // app/Resources/views/Emails/registration.html.twig
                        'Emails/User/autosell.html.twig', 
                        array(
                            'generali' => $generalData,
                    'prodotti' => $elenco)
                ), 'text/html'
        );

        $this->get('mailer')->send($message);
        */
        //MAIL PER AMMINISTRATORE
        //MAIL PER CLIENTE
        $message = \Swift_Message::newInstance()
                ->setSubject($oggettoAdmin)
                ->setFrom('info@reflexmania.it')
                ->setTo('info@reflexmania.it')
              //  ->setReplyTo($generalData[2])
                ->attach(Swift_Attachment::fromPath($allegato))
                ->setBody(
                $this->renderView(
                        'ritiri/Email/booked.html.twig', 
                        array()
                ), 'text/html'
        );

        $this->get('mailer')->send($message);
    }
    
    
   

}
