<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Ritiri;
use AppBundle\Entity\Permuta;
use AppBundle\Form\Ritiri\FormRitiro;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Entity;
use AppBundle\Repository\EntityRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Form\FormError;
//JSON
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
//FILE DOWNLOAD
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\File\MimeType\FileinfoMimeTypeGuesser;
use Symfony\Component\HttpFoundation\File\File;
use AppBundle\Service\Downloader;
//SERVICE
use AppBundle\Service\ApiPDF;

class RitiriController extends Controller {
    //////////////////////////////////////////////////////////////////
// PRENOTO UN RITIRO SENZA RICHIESTA DI PERMUTA
///////////////////////////////////////////////////////////////////////

    /**
     * @Route("/ritiri", name="ritiri")
     */
    public function prenotaAction(Request $request) {
        $ritiriModel = new Ritiri();
        $formRitiro = $this->createForm(FormRitiro::class, $ritiriModel);
        $formRitiro->handleRequest($request);
        if ($formRitiro->isSubmitted()) {
            //CHECK DELLO ZIP!
            $params = $formRitiro->getData();
            $cap = $params->getVaocar();
            $sinonimo = strtoupper($params->getVaolor());
            //CERCO FRA LOCALITA
            $em = $this->getDoctrine()->getManager();
            $loc = array();
            ////////////////////////
            // CERCO LA LOCALITA A PARTIRE DAL SINONIMO
            ////////////////////////
            //////
            $em = $this->getDoctrine()->getManager();
            $entities = $em->getRepository('AppBundle:Localita')->findLocalita($sinonimo);
            //$loc = $entities->getResult();
            $capres = array();
            $contatore = 0;
            $check = false;

            foreach ($entities as $result) {
                $result['cap'];
                if ($result['cap'] == $cap)
                    $control = true;
                else
                    $control = false;
                $check = $check || $control;
                $contatore++;
            }

            if (!$check) {
                $formRitiro->get('vaolor')->addError(new FormError('ERRORE:LOCALITA NON CONGRUENTE CON IL CAP'));
            } else {
                if ($formRitiro->isValid()) {
                    $ritiroEntity = $formRitiro->getData();

                    //ESEGUO AZIONE DI INSERIMENTO DATABASE
                    //INSERISCO DATI NEL DATABASE
                    $ritiriModel->setGestito(FALSE);
                    $ritiriModel->setVaorer($ritiroEntity->getVaorsr());
                    $ritiriModel->setVaoter($ritiroEntity->getVaosmsr());
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($ritiriModel);
                    $em->flush();
                    //IMPOSTO IL CODICE UNIVOCO RICERCABILE
                    $lastritiro = $ritiriModel->getId();
                    $univocoBRT = "RXMN-" . $lastritiro;
                    $ritiro = $em->getRepository(Ritiri::class)->find($lastritiro);
                    $ritiro->setCodbartolini($univocoBRT);
                    $em->flush();

                    $generalData['email'] = $ritiriModel->getVaoemlr();
                    $generalData['data'] = $ritiriModel->getVaodar();
                    $generalData['nome'] = $ritiriModel->getVaorsr();
                    // ESEGUO LE OPERAZIONI DI INVIO MAIL E CREAZIONE PDF


                    $apiController = $this->get('api_controller');
                    $generalData['ritiro'] = $univocoBRT;

                    $filename = $univocoBRT;
                    $this->createpdfEmptyAction($request, $filename, $ritiriModel);
                    $path = $this->getParameter('pdf_directory');
                    //FILE RICEVUTA  
                    $file_with_path = $this->getParameter('pdf_directory') . $filename . '.pdf';

                    //MANDA MAIL CON PDF
                    $this->ritirimailAction($generalData, $file_with_path);


                    //PAGINA DI SUCCESSO
                    return $this->render('ritiri/successRitiri.html.twig', array(
                                'codiceBartolini' => $univocoBRT,
                                'filename' => $file_with_path
                    ));
                }
            }
        }


        return $this->render('ritiri/ritiro.html.twig', array(
                    'formritiro' => $formRitiro->createView(),
        ));
    }

    /**
     * @Route("/success", name="success")
     */
    public function successAction(Request $request) {
        return $this->render('customer/ritiri/success.html.twig');
    }

    /**
     * @Route("/sendmail", name="sendmail")
     */
    public function sendmailAction($modello, $univoco) {
        $message = \Swift_Message::newInstance()
                ->setSubject('RITIRO PRENOTATO')
                ->setFrom('system@reflexmania.it')
                ->setTo('permuta@reflexmania.it')
                ->setBody(
                $this->renderView(
                        // app/Resources/views/Emails/registration.html.twig
                        'emails/email.html.twig', array('univoco' => $univoco,
                    'dati' => $modello)
                ), 'text/html')


        ;
        $this->get('mailer')->send($message);
    }

//////////////////////////////////////////////////////////////////
// PRENOTO UN RITIRO DOPO LA RICHIESTA DI PERMUTA
///////////////////////////////////////////////////////////////////////

    /**
     * @Route("/booknow", name="booknow")
     */
    public function afterpermutaAction(Request $request) {
        $apiController = $this->get('api_controller');
        //RECUPERO DATI PERMUTA
        $em = $this->getDoctrine()->getManager();
        $idPermuta = $request->query->get('id');
        $generalData = $apiController->GetPermuta($em, $idPermuta);
        //$entDati = $em->getRepository('AppBundle:Permuta')->getData($idPermuta);
        $nome = $generalData['nome'];
        $cognome = $generalData['cognome'];
        $email = $generalData['email'];
        $telefono = $generalData['telefono'];
        $referente = '' . $nome . ' ' . $cognome;


//RECUPERO ID PERMUTA

        $ritiriModel = new Ritiri();
        $formRitiro = $this->createForm(FormRitiro::class, $ritiriModel);

        //PRESETTO I DATI 
        $formRitiro->get('vaorsr')->setData($referente);
        $formRitiro->get('vaoemlr')->setData($email);
        $formRitiro->get('vaosmsr')->setData($telefono);
        //
        $formRitiro->handleRequest($request);
        if ($formRitiro->isSubmitted()) {
            //CHECK DELLO ZIP!
            $params = $formRitiro->getData();
            $cap = $params->getVaocar();
            $sinonimo = strtoupper($params->getVaolor());
            //CERCO FRA LOCALITA
            $em = $this->getDoctrine()->getManager();
            $loc = array();
         // CERCO LA LOCALITA A PARTIRE DAL SINONIMO
            ////////////////////////
            //////
            $em = $this->getDoctrine()->getManager();
            $entities = $em->getRepository('AppBundle:Localita')->findLocalita($sinonimo);
            //$loc = $entities->getResult();
            $capres = array();
            $contatore = 0;
            $check = false;

            foreach ($entities as $result) {
                $result['cap'];
                if ($result['cap'] == $cap)
                    $control = true;
                else
                    $control = false;
                $check = $check || $control;
                $contatore++;
            }

            if (!$check) {
                $formRitiro->get('vaolor')->addError(new FormError('ERRORE:LOCALITA NON CONGRUENTE CON IL CAP'));
            }  else {
                if ($formRitiro->isValid()) {
                    $ritiroEntity = $formRitiro->getData();
                    $ritiriModel->setVaoter($ritiroEntity->getVaosmsr());
                    //ESEGUO AZIONE DI INSERIMENTO DATABASE
                    //INSERISCO DATI NEL DATABASE
                    $ritiriModel->setGestito(FALSE);
                    $ritiriModel->setVaorer($ritiroEntity->getVaorsr());
                    $ritiriModel->setIdPermuta($idPermuta);
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($ritiriModel);
                    $em->flush();
                    //IMPOSTO IL CODICE UNIVOCO RICERCABILE
                    $lastritiro = $ritiriModel->getId();
                    $univocoBRT = "RXMN-" . $lastritiro;
                    $ritiro = $em->getRepository(Ritiri::class)->find($lastritiro);
                    $ritiro->setCodbartolini($univocoBRT);
                    $em->flush();
                   
                    //AGGIORNO BOOLEAN CHECK RITIRO
                    $permuta = $em->getRepository(Permuta::class)->find($idPermuta);
                    $permuta->setCheckRitiro(1);
                    $em->flush();
                    $apiController = $this->get('api_controller');
                    $generalData = $apiController->GetPermuta($em, $idPermuta);
                    $elenco = $generalData['prodotti'];
                    $generalData['ritiro'] = $univocoBRT;
                    $generalData['permuta'] = $idPermuta;
                    $generalData['data'] = $ritiriModel->getVaodar();

                    $filename = $univocoBRT;
                    $this->createpdfAction($request, $filename, $ritiriModel, $elenco);
                    $path = $this->getParameter('pdf_directory');
                    //FILE RICEVUTA  
                    $file_with_path = $this->getParameter('pdf_directory') . $filename . '.pdf';
                    //INVIO MAIL 
                    $this->ritirimailAction($generalData, $file_with_path);

                    return $this->render('ritiri/successRitiri.html.twig', array(
                                'codiceBartolini' => $univocoBRT,
                                'filename' => $file_with_path
                    ));
                }
            }
        }


        return $this->render('ritiri/booknow.html.twig', array(
                    'idPermuta' => $idPermuta,
                    'formritiro' => $formRitiro->createView(),
        ));
    }

    //MANDO MAIL RITIRO CON ALLEGATO
    public function ritirimailAction($generalData, $file) {

        $oggettoAdmin = "Ritiro Prenotato - ". $generalData['nome'];
        $oggettoUser = "Il tuo ritiro è stato prenotato!";

        //MAIL PER CLIENTE
        $message = \Swift_Message::newInstance()
                ->setSubject($oggettoUser)
                ->setFrom('permuta@reflexmania.it')
                ->setTo($generalData['email'])
                ->attach(\Swift_Attachment::fromPath($file))
                ->setBody(
                $this->renderView(
                        // app/Resources/views/Emails/registration.html.twig
                        'Emails/Ritiri/User/ritiroprenotato.html.twig', array('generali' => $generalData)
                ), 'text/html'
        );

        $this->get('mailer')->send($message);

        //MAIL PER AMMINISTRATORE
        //MAIL PER CLIENTE
        $message = \Swift_Message::newInstance()
                ->setSubject($oggettoAdmin)
                ->setFrom('permuta@reflexmania.it')
                ->setTo('permuta@reflexmania.it')
                ->setBody(
                        $this->renderView(
                                'ritiri/Email/booked.html.twig', array('generali' => $generalData)
                        ), 'text/html'
                )
                ->attach(\Swift_Attachment::fromPath($file));

        $this->get('mailer')->send($message);
    }

    public function createpdfAction(Request $request, $filename, $generalData, $prodotti) {
        $snappy = $this->get('knp_snappy.pdf');

        $html = $this->renderView('PDF/pdf.html.twig', array(
            'title' => 'RICEVUTA REFLEXMANIA',
            'anagrafica' => $generalData,
            'prodotti' => $prodotti,
        ));
        $intro = $this->renderView('PDF/intro.html.twig', array(
            'title' => 'Istruzioni',
        ));

        $pdfFolder = $this->getParameter('pdf_directory');


        //     $snappy->generateFromHtml($html, $pdfFolder . time() . '.pdf');
        return new Response(
                $snappy->generateFromHtml([$intro, $html], $pdfFolder . $filename . '.pdf'), 200, array(
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="' . $filename . '.pdf"'
                )
        );
    }

    public function createpdfEmptyAction(Request $request, $filename, $generalData) {
        $snappy = $this->get('knp_snappy.pdf');

        $html = $this->renderView('PDF/pdfEmpty.html.twig', array(
            'title' => 'Ricevuta di Acquisto',
            'anagrafica' => $generalData,
        ));

        $intro = $this->renderView('PDF/intro.html.twig', array(
            'title' => 'Istruzioni',
        ));
        $lettera = $this->renderView('PDF/lettera.html.twig', array(
            'title' => 'Lettera',
        ));
        $pdfFolder = $this->getParameter('pdf_directory');


        //     $snappy->generateFromHtml($html, $pdfFolder . time() . '.pdf');
        return new Response(
                $snappy->generateFromHtml([$intro, $html, $lettera], $pdfFolder . $filename . '.pdf'), 200, array(
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="' . $filename . '.pdf"'
                )
        );
    }

    /**
     * @Route("/downloadfile", name="downloadfile")
     */
    public function downloadfileAction(Request $request) {
        $nomefile = $request->query->get('nomefile');
        $response = new BinaryFileResponse($nomefile);
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);

        return $response;
    }
    
    
    

}

?>