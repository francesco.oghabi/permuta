<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
//use Symfony\Component\HttpFoundation\BinaryFileResponse;
//use Symfony\Component\HttpFoundation\ResponseHeaderBag;



use AppBundle\Entity\Ritiri;
use AppBundle\Form\Ritiri\FormRitiro;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Entity;
use AppBundle\Repository\EntityRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Form\FormError;


//JSON
use Symfony\Component\Serializer\Serializer;

use  Symfony\Component\Serializer\Encoder\XmlEncoder;
use  Symfony\Component\Serializer\Encoder\JsonEncoder;
use  Symfony\Component\Serializer\Normalizer\ObjectNormalizer;



//FILE DOWNLOAD
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;


use Symfony\Component\HttpFoundation\File\MimeType\FileinfoMimeTypeGuesser;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

//use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * @Route(service="api_controller")
 */
class ApiController extends Controller
{
    
  

//DOWNLOAD FILE
    public function downloadFile($filename){
         $response = new BinaryFileResponse($filename);
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);
        
            return $response;
   // Generate response
      /* 
$response = new Response();

// Set headers
$response->headers->set('Cache-Control', 'private');
$response->headers->set('Content-type', mime_content_type($filename));
$response->headers->set('Content-Disposition', 'attachment; filename="' . basename($filename) . '";');
$response->headers->set('Content-length', filesize($filename));

// Send headers before outputting anything
$response->sendHeaders();

$response->setContent(file_get_contents($filename));
*/
        /*
        $response = new BinaryFileResponse($filename);
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);*/
     
        
        
}


//RECUPERA TUTTI I DATI DI UNA PERMUTA
// E METTILI IN UN ARRAY DA LAVORARE
 public function getPermuta($em,$idPermuta){
                $generalData['totale']=0;
                $permutaEntity = $em->getRepository('AppBundle:Permuta')->getData($idPermuta);
              $generalData['nome'] =$permutaEntity[0]->getNome();
              $generalData['cognome'] =$permutaEntity[0]->getCognome();
              $generalData['telefono'] =$permutaEntity[0]->getTelefono();
              $generalData['email'] =$permutaEntity[0]->getEmail();
              //$generalData['prodotti']=;
              $prodotti=$permutaEntity[0]->getProdotti();
              $elenco = array();
              //CARICO I PRODOTTI
              foreach ($prodotti as $prodotto) {
                  //Variabili helper
                $idProdotto = $prodotto->getId();
               //  $condizioneID = $prodotto->getCondizioneId()->getId();
                //Variabil finali nel vettore
                $modello = $prodotto->getModello();
              //  $condizione = $prodotto->getCondizioneId()->getCondizione();
               $condizioneID = $prodotto->getCondizioneId();
                 //RECUPERA PREZZO
            //     $entity = $em->getRepository('AppBundle:Prezzi')->findPrice($idProdotto, $condizioneID);
                    
                $entity = $em->getRepository('AppBundle:Condizioni')->getCodice($condizioneID);
                $condizione = $entity[0]->getCondizione();
                $prezzo = $prodotto->getPrezzo();
                $generalData['totale'] = $generalData['totale'] + $prezzo;
                
                //
                
                $product = array($modello, $condizione,$prezzo,$idProdotto);
                //$product = array($modello, $condizione,$prezzo,$idProdotto);
                array_push($elenco, $product);}
                
                
                $generalData['prodotti'] = $elenco;
                return $generalData;
     
    
 
}



public function mergePDF($f2,$f1,$out){

    $process = new Process('gs -dBATCH -DNOPAUSE -q -sDEVICE=pdfwrite -dPDFSETTINGS=/prepress -sOutputFile='.$out. ' '.$f1 . ' '. $f2);
    $process->run();
    if (!$process->isSuccessful()) {
        throw new ProcessFaiedException($process);
       
    }
    echo $process->getOutput();
 
}
}


