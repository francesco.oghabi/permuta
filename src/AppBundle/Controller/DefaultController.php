<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Permuta;
use AppBundle\Form\FormPermuta;
use AppBundle\Form\ProdottiType;
use AppBundle\Entity\Prodotti;
use AppBundle\Entity\Buy;
use AppBundle\Form\BuyType;
use AppBundle\Entity\Pagamento;
use AppBundle\Form\FormPagamento;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Entity;
use AppBundle\Repository\EntityRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
//JSON
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
//API REST
use Lsw\ApiCallerBundle\Call\HttpGetJson;
use Lsw\ApiCallerBundle\Call\HttpPostJson;
use Lsw\ApiCallerBundle\Call\HttpPostJsonBody;

class DefaultController extends Controller {

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request) {
        //VISUALIZZAZION FORM
        $entityManager = $this->getDoctrine()->getManager();
        $pemutaEntity = new Permuta();
        $permutaForm = $this->createForm(FormPermuta::class, $pemutaEntity);

        //FORM SUBMISSION
        $permutaForm->handleRequest($request);

        if ($permutaForm->isSubmitted() && $permutaForm->isValid()) {
          
            $pemutaEntity = $permutaForm->getData();


            $dati = $permutaForm->getData();

            foreach ($pemutaEntity->getProdotti() as $prodotti) {
                $prodotti->setIdPermuta($pemutaEntity);
            }


            $elencoRichieste = array();
            $prodottiRichiesti = $pemutaEntity->getWhatbuy();
            $Richiesti = array();
            $counter = count($pemutaEntity->getWhatbuy());
            if ($counter != 0) {
                $Richiesti = $this->trovototaleAction($pemutaEntity);
                //INSERISCO L'ID PERMUTA NEI PRODOTTI RICHIESTI
                foreach ($pemutaEntity->getWhatbuy() as $whatbuy) {
                    $whatbuy->setIdPermuta($pemutaEntity);
                }
                $stovendendo = $Richiesti['totale'];
                $checkVendita = $Richiesti['check'];
                $elencoRichieste = $Richiesti['elenco'];
            }
         
            //LANCIO VALUTAZIONE SUI PRODOTTI CHE VUOLE VENDERE
            $DaPermutare = $this->valutazioneAction($dati);
           
            $stoacquistando = $DaPermutare['totale'];

            $checkAcquisto = $DaPermutare['check'];
       
            $generalData = $DaPermutare['generali'];
            $elenco = $DaPermutare['elenco'];

          
            //CREO LA NUOVA PERMUTA
            $lastPermuta = $entityManager->getRepository('AppBundle:Permuta')->findLast();
            if ($lastPermuta) $lastId = $lastPermuta->getId();
            else $lastId=0;
            $rifPermuta = $lastId + 1;
            array_unshift($generalData, $rifPermuta);
            


            //MANDO LE MAIL
            //CASO FAVOREVOLE PERMTUA
            //TUTTI I PRODOTTI SONO PRESENTI NEL DATABASE E HO TUTTI I PREZZI
            //SETTATI CORRETTAMENTE
            if ($counter != 0 && $checkAcquisto && $checkVendita) {
                //FUNZIONI DI MODIFICA DEI PREZZI
                $this->setprezziAction($pemutaEntity, $elenco);
                //CALCOLO VALUTAZIONE DI PERMUTA
                $addPerc = $this->variabilitaAction($stoacquistando, $stovendendo);   //echo $addPerc;
                $this->updateprezziAction($pemutaEntity, $addPerc);
                $this->autotradeinAction($generalData, $elenco, $addPerc, $elencoRichieste);
                 $pemutaEntity->setIsAuto(1);

               
            }
            //I PRODOTTI CHE IL CLIENTE VUOLE PERMUTARE NON LI CONOSCO
            //MA QUELLO CHE VUOLE COMPRARE SI
            if ($counter != 0 && $checkAcquisto && !$checkVendita) {
                //VOGLIO PERMUTARE QUALCOSA MA VADO IN VALUTAZIONE MANUALE
                $this->mantradein2Action($generalData, $elenco, $elencoRichieste);
                 $pemutaEntity->setIsAuto(0);
                //VADO IN CONTROLLO MANUALE
            }
            
             if ($counter != 0 && $checkVendita) {
                //VOGLIO PERMUTARE QUALCOSA MA VADO IN VALUTAZIONE MANUALE
                $this->mantradeinAction($generalData, $elenco, $elencoRichieste);
                 $pemutaEntity->setIsAuto(0);
                //VADO IN CONTROLLO MANUALE
            }
            
            //SOLA VENDITA SENZA PERMUTA GESTITA CORRETTAMENTE IN AUTOMATICO
            //
            if ($checkAcquisto && $counter == 0) {
                $this->setprezziAction($pemutaEntity, $elenco);
                $this->autosellAction($generalData, $elenco);
                 $pemutaEntity->setIsAuto(1);
            }

            if (!$checkAcquisto && $counter == 0) {
                //NON RIESCO A FARE UNA VALUTAZIONE AUTOMATICA
                $this->mansellAction($generalData, $elenco);
                 $pemutaEntity->setIsAuto(0);
            }

            // SETTO RITIRO DA GESTIRE
             $pemutaEntity->setCheckRitiro(0);
            $entityManager->persist($pemutaEntity);
            $entityManager->flush();
            //FINE INVIO MAIL
              //PAGINA DI SUCCESSO
                    return $this->render('permuta/successPermuta.html.twig', array(
                              
                    ));
          
        }


        return $this->render('permuta/index.html.twig', array(
                    'form' => $permutaForm->createView(),
        ));
    }

    public function setprezziAction($permutaEntity, $elencoProdotti) {
        $index = 0;
        foreach ($permutaEntity->getProdotti() as $prodotto) {
            //echo $index;
            $prodotto->setPrezzo($elencoProdotti[$index][3]);
            $index++;
        }

        return $permutaEntity;
    }

    public function updateprezziAction($permutaEntity, $percentuale) {
        
        foreach ($permutaEntity->getProdotti() as $prodotto) {
             $prezzoOLD = $prodotto->getPrezzo();
              $prezzoNEW = $prezzoOLD + ($prezzoOLD * $percentuale / 100);
           $prodotto->setPrezzo($prezzoNEW);
         
        }

        return $permutaEntity;
    }
        
     
   
    public function trovototaleAction($entity) {
        $risultato['check'] = true;
        $em = $this->getDoctrine()->getManager();
        $elenco = array();
        $checkautoV = true;
        $checkIdProdottoV = true;
        $prezzoVenditaTotale = 0;

        //   $tutti = $entity->getWhatbuy()[1]->getBuyIdMod();
        $counter = count($entity->getWhatbuy());
        foreach ($entity->getWhatbuy() as $prodottiRichiesti) {
            $idProdotto = $prodottiRichiesti->getBuyIdMod();
            $modello = $prodottiRichiesti->getBuyMod();

            $buy = 'BASE';

            if (!$idProdotto) {
                $risultato['check'] = false;
                $idProdotto = NULL;
                $product = array($idProdotto, $modello);
                array_push($elenco, $product);
            } else {
                //GESTISCO API
                $parametri = array('id' => $idProdotto, 'condizione' => $buy);
                $url = $this->getParameter('url_rest_server') . 'venditaprice?';
                $resPrezzi = $this->get('api_caller')->call(new HttpPostJson($url, $parametri));
               $arr = json_decode($resPrezzi, true);

                //ACCESSO ALL ARRAY JSON
               
                $prezzo = $arr[0]["prezzo"];


                $prezzoVend = $prezzo;
                $product = array($idProdotto, $modello, $prezzoVend);
                array_push($elenco, $product);
                if ($prezzoVend == 0)
                    $risultato['check'] = false;
                else
                    $prezzoVenditaTotale = $prezzoVenditaTotale + $prezzoVend;
            }
        }
        $risultato['elenco'] = $elenco;
        $risultato['totale'] = $prezzoVenditaTotale;
        return $risultato;
    }

    //ESEGUO VALUTAZIONE DEL MATERIALE CHE DEVE RIENTRARE

    public function valutazioneAction($permuta) {
        //echo $permuta;
        $em = $this->getDoctrine()->getManager();
        $risultato['check'] = true;
        $risultato['totale'] = 0;
        $nome = $permuta->getNome();
        $cognome = $permuta->getCognome();
        $email = $permuta->getEmail();
        $telefono = $permuta->getTelefono();
        $prodotti = $permuta->getProdotti();

        //RECUPERO I PRODOTTI CHE IL CLIENTE VUOLE VENDERE
        $elenco = array();
        $checkauto = true;
        $checkIdProdotto = true;
        $prezzoAcquistoTotale = 0;
        $prezzo = 0;
        $idProdotto = 0;
        $counter = count($permuta->getProdotti());
        //echo $counter;
        $generalData = array($nome, $email, $telefono, $cognome);
        $risultato['generali'] = $generalData;
        if ($prodotti) {
            //$tutti = $permuta->getProdotti()[1]->getProductid();
            $counter = count($permuta->getProdotti());

            foreach ($prodotti as $prodotto) {
                $modello = $prodotto->getModello();
               $condizioneId = $prodotto->getCondizioneId();
                $idProdotto = $prodotto->getProductid();
                //SE IL PRODOTTO HA UN ID ALLORA CE L'HO IN DATABASE
                if ($idProdotto) {

                    
                    //RECUPERO CODICE INVOICEX DI CONDIZIONE
                    $CondEntity = $em->getRepository('AppBundle:Condizioni')->getCodice($condizioneId);
                    $codInvCond = $CondEntity[0]->getCodice();
                    
                    $condizione = $CondEntity[0]->getCondizione();
                    
                    //TEST API SU ALTRO SERVER
                  
                    $parametri = array('id' => $idProdotto, 'condizione' => $codInvCond);
                    $url = $this->getParameter('url_rest_server') . 'findprice?';
                    $resPrezzi = $this->get('api_caller')->call(new HttpPostJson($url, $parametri));
            
                    $arr = json_decode($resPrezzi, true);

                    //ACCESSO ALL ARRAY JSON

                    $prezzo = $arr[0]["prezzo"];
                    $risultato['totale'] = $risultato['totale'] + $prezzo;
                    $product = array($idProdotto, $modello, $condizione, $prezzo);
                    array_push($elenco, $product);
                } else {
                    $risultato['check'] = false;
                    $idProdotto = NULL;
                    $product = array($idProdotto, $modello, $condizioneId);
                    array_push($elenco, $product);
                }
            }
         

            $risultato['elenco'] = $elenco;
            return $risultato;
        }
    }

    //MANDO MAIL AUTOMATICA. --> IL SISTEMA HA FUNZIONATO CORRETTAMENTE
    public function autotradeinAction($generalData, $elenco, $percentuale, $richieste) {
        $oggettoAdmin = "PERMUTA GESTITA-" . $generalData[1] . " " . $generalData[4] . "";
        $oggettoUser = "Vendi subito la tua attrezzatura!";
        //MAIL AL CLIENTE
        $name = 'Francesco';
        $message = \Swift_Message::newInstance()
                ->setSubject($oggettoUser)
                ->setFrom('permuta@reflexmania.it')
                ->setTo($generalData[2])
                ->setBody(
                $this->renderView(
                        // app/Resources/views/Emails/registration.html.twig
                        'Emails/User/autotradein.html.twig', array('generali' => $generalData,
                    'prodotti' => $elenco,
                    'percentuale' => $percentuale,
                    'richieste' => $richieste)
                ), 'text/html'
        );

        $this->get('mailer')->send($message);


        //MAIL ALL AMMINISTRATORE

        $name = 'Francesco';
        $message = \Swift_Message::newInstance()
                ->setSubject($oggettoAdmin)
                ->setFrom('permuta@reflexmania.it')
                ->setTo('permuta@reflexmania.it')
                ->setReplyTo($generalData[2])
                ->setBody(
                $this->renderView(
                        // app/Resources/views/Emails/registration.html.twig
                        'Emails/Admin/autotradein.html.twig', array('generali' => $generalData,
                    'prodotti' => $elenco,
                    'percentuale' => $percentuale,
                    'richieste' => $richieste)
                ), 'text/html'
        );

        $this->get('mailer')->send($message);
    }

    public function autosellAction($generalData, $elenco) {
        $oggettoAdmin = "VENDITA GESTITA-" . $generalData[1] . " " . $generalData[4] . "";
        $oggettoUser = "Vendi subito la tua attrezzatura!";

        //MAIL PER CLIENTE
        $message = \Swift_Message::newInstance()
                ->setSubject($oggettoUser)
                ->setFrom('permuta@reflexmania.it')
                ->setTo($generalData[2])
                ->setBody(
                $this->renderView(
                        // app/Resources/views/Emails/registration.html.twig
                        'Emails/User/autosell.html.twig', array(
                    'generali' => $generalData,
                    'prodotti' => $elenco)
                ), 'text/html'
        );

        $this->get('mailer')->send($message);

        //MAIL PER AMMINISTRATORE
        //MAIL PER CLIENTE
        $message = \Swift_Message::newInstance()
                ->setSubject($oggettoAdmin)
                ->setFrom('permuta@reflexmania.it')
                ->setTo('permuta@reflexmania.it')
                ->setReplyTo($generalData[2])
                ->setBody(
                $this->renderView(
                        // app/Resources/views/Emails/registration.html.twig
                        'Emails/Admin/autosell.html.twig', array(
                    'generali' => $generalData,
                    'prodotti' => $elenco)
                ), 'text/html'
        );

        $this->get('mailer')->send($message);
    }

     //VADO IN VALUTAZIONE MANUALE.
    //IL CLIENTE HA RICHIESTO UNA PERMUTA MA NON HO I VALORI
    //PER GESTIRLO IN MODO AUTOMATICO
    
    
    public function mantradein2Action($generalData, $elenco, $richiesti) {
      
        
        
          //MAIL ALL AMMINISTRATORE
        $oggettoAdmin= "PERMUTA DA GESTIRE";
        $message = \Swift_Message::newInstance()
                ->setSubject($oggettoAdmin)
                ->setFrom('permuta@reflexmania.it')
                ->setTo('permuta@reflexmania.it')
                ->setReplyTo($generalData[2])
                ->setBody(
                $this->renderView(
                        // app/Resources/views/Emails/registration.html.twig
                        'Emails/Admin/mantradein2.html.twig', array('generali' => $generalData,
                    'prodotti' => $elenco,
                              'richiesti' => $richiesti
                        )
                ), 'text/html'
        );

        $this->get('mailer')->send($message);
    }

    
    
    //VADO IN VALUTAZIONE MANUALE.
    //IL CLIENTE HA RICHIESTO UNA PERMUTA MA NON HO I VALORI
    //PER GESTIRLO IN MODO AUTOMATICO
    public function mantradeinAction($generalData, $elenco, $richiesti) {
        $oggettoUser = "La tua Valutazione da ReflexMania";
        $oggettoAdmin = "PERMUTA DA GESTIRE-" . $generalData[1] . " " . $generalData[4] . "";


        //MAIL ALL AMMINISTRATORE

        $message = \Swift_Message::newInstance()
                ->setSubject($oggettoAdmin)
                ->setFrom('permuta@reflexmania.it')
                ->setTo('permuta@reflexmania.it')
                ->setReplyTo($generalData[2])
                ->setBody(
                $this->renderView(
                        // app/Resources/views/Emails/registration.html.twig
                        'Emails/Admin/mantradein.html.twig', array('generali' => $generalData,
                    'prodotti' => $elenco,
                    'richiesti' => $richiesti)
                ), 'text/html'
        );

        $this->get('mailer')->send($message);
        
        
          //MAIL ALL AMMINISTRATORE
        $oggettoAdmin= "VENDITA DA GESTIRE";
        $message = \Swift_Message::newInstance()
                ->setSubject($oggettoAdmin)
                ->setFrom('permuta@reflexmania.it')
                ->setTo('permuta@reflexmania.it')
                ->setReplyTo($generalData[2])
                ->setBody(
                $this->renderView(
                        // app/Resources/views/Emails/registration.html.twig
                        'Emails/Admin/mantradein.html.twig', array('generali' => $generalData,
                    'prodotti' => $elenco,
                              'richiesti' => $richiesti
                        )
                ), 'text/html'
        );

        $this->get('mailer')->send($message);
    }

    //IL CLIENTE HA RICHIESTO UNA VENDITA MA NON HO I VALORI
    //PER GESTIRLO IN MODO AUTOMATICO
    public function mansellAction($generalData, $elenco) {


        //MAIL ALL AMMINISTRATORE
        $oggettoAdmin= "VENDITA DA GESTIRE";
        $message = \Swift_Message::newInstance()
                ->setSubject($oggettoAdmin)
                ->setFrom('permuta@reflexmania.it')
                ->setTo('permuta@reflexmania.it')
                ->setReplyTo($generalData[2])
                ->setBody(
                $this->renderView(
                        // app/Resources/views/Emails/registration.html.twig
                        'Emails/Admin/mansell.html.twig', array('generali' => $generalData,
                    'prodotti' => $elenco,
                        )
                ), 'text/html'
        );

        $this->get('mailer')->send($message);
    }

    /**
     * @Route("/fatti-pagare", name="fattipagare")
     */
    public function fattipagareAction(Request $request) {
        //ID RITIRO
        $idRitiro = $request->query->get('id');
        //VISUALIZZAZION FORM
        $pagamentoEntity = new Pagamento();
        $pagamentoForm = $this->createForm(FormPagamento::class, $pagamentoEntity);

        //FORM SUBMISSION
        $pagamentoForm->handleRequest($request);

        if ($pagamentoForm->isSubmitted() && $pagamentoForm->isValid()) {

            $pagamentoEntity = $pagamentoForm->getData();


            $dati = $pagamentoForm->getData();

            $pagamentoEntity->setIdRitiro($idRitiro);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($pagamentoEntity);
            $entityManager->flush();
             return $this->render('Pagamento/succPagamento.html.twig', array(
                   
        ));
        }


        return $this->render('Pagamento/pagamento.html.twig', array(
                    'form' => $pagamentoForm->createView(),
        ));
    }

    public function variabilitaAction($totaleAcquisto, $totaleVendita) {   //echo $totaleVendita;
        //echo $totaleAcquisto;
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:Scaglioni')->findInterval($totaleVendita);
        //TROVO ID INTERVALLO
        $idScaglione = $entity[0]->getId();
        $rapporto = $totaleVendita / $totaleAcquisto;
        $entity2 = $em->getRepository('AppBundle:RapportiBS')->findInterval($rapporto);
        $idRapporto = $entity2[0]->getId();
        $entity3 = $em->getRepository('AppBundle:Costomkt')->getPerc($idRapporto, $idScaglione);
        $percentuale = $entity3[0]->getVariabilita();
        // $test = $totaleAcquisto;
        return $percentuale;
    }

}
