/*------------ EZ Accordion ------------
function ezAccordion(sSelector) {
   var a = this;
   var b = this;

       a.container   = $(sSelector);
       a.item        = a.container.find('.accordion__section');
       a.itemTitle   = a.container.find('.accordion__titlebox');
       a.itemContent = a.container.find('.accordion__content');

       a.closeAccordionSection = function () {
          a.itemTitle.removeClass('accordion__titlebox_active');
          a.itemContent.slideUp(300);
       }

       a.openAccordionSection = function () {
          var currentAttrValue = $(this).data('href');
		 $(currentAttrValue).slideToggle(300);

              if($(this).hasClass('accordion__titlebox_active')) {
                  a.itemTitle.removeClass('accordion__titlebox_active');
				   
              } else {
                  //a.itemTitle.removeClass('accordion__titlebox_active');
                  // Add active class to section title
                  $(this).addClass('accordion__titlebox_active');
                  // Open up the hidden content panel
                  
              }
       }

       b.itemTitle.bind('click', b.openAccordionSection);
	   b.container   = $(sSelector);
       b.item        = b.container.find('.accordion__section');
       b.itemTitle   = b.container.find('.accordion__titlebox');
       b.itemContent = b.container.find('.accordion__content');

       b.closeAccordionSection = function () {
          b.itemTitle.removeClass('accordion__titlebox_active');
          b.itemContent.slideUp(300);
       }

       b.openAccordionSection = function () {
          var currentAttrValue = $(this).data('href');
		 $(currentAttrValue).slideToggle(300);

              if($(this).hasClass('accordion__titlebox_active')) {
                  b.itemTitle.removeClass('accordion__titlebox_active');
				   
              } else {
                  //a.itemTitle.removeClass('accordion__titlebox_active');
                  // Add active class to section title
                  $(this).addClass('accordion__titlebox_active');
                  // Open up the hidden content panel
                  
              }
       }
}


------------------------------------------*/
