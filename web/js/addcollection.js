/* 
 * PRODOTTI CHE VUOLE PERMUTARE
 */

        var $collectionHolder;
        // setup an "add a tag" link
        var $addTagLink = $('<button class="border-btn add_tag_link" type="button" data-duplicate-remove="demo">Aggiungi un Prodotto</button>');
     //   var $buyTagLink = $('<a href="#" class="add_tag_link">Aggiungi un prodotto</a>');
        var $newLinkLi = $('<div></div>').append($addTagLink);

//what wanna buy

        var $BuyHolder;
        var $addBuyLink = $('<button class="border-btn add_tag_link" type="button" data-duplicate-remove="demo">Aggiungi un Prodotto</button>');
        var $newBuyLink = $('<div></div>').append($addBuyLink);
        
// setup an "add a tag" link
       // var $addTagLink = $('<a href="#" class="add_tag_link">Aggiungi un prodotto</a>');
        //var $addTagLink = $('<a href="#" class="add_tag_link">Aggiungi un prodotto</a>');

//        var $newLinkLi = $('<li></li>').append($addTagLink);



jQuery(document).ready(function() {
    
    //HIDE SHOW PRODUCTS
     $("#sell").click(function(){$("#buying").hide();});
     $("#trade").click(function(){$("#buying").show(); });
    
    
    // Get the ul that holds the collection of tags
    $collectionHolder = $('div.prodotti');
    $BuyHolder =$('div.whatbuy');
    // add the "add a tag" anchor and li to the tags ul
    $collectionHolder.append($newLinkLi);
    $BuyHolder.append($newBuyLink);
    // count the current form inputs we have (e.g. 2), use that as the new
    // index when inserting a new item (e.g. 2)
    $collectionHolder.data('index', $collectionHolder.find(':input').length);
    $BuyHolder.data('index', $BuyHolder.find(':input').length);

    $addTagLink.on('click', function(e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();

        // add a new tag form (see next code block)
        addTagForm($collectionHolder, $newLinkLi);
        
    });
     $addBuyLink.on('click', function(e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();

        // add a new tag form (see next code block)
        addTagForm($BuyHolder, $newBuyLink);
    });
    
    
    /*****
     * 
     * @param {type} $collectionHolder
     * @param {type} $newLinkLi
     * @returns {undefined}
     * 
     * 
     */
    
    
    
    
    // FINE AUTOCOMPLETE
});


function addTagForm($collectionHolder, $newLinkLi) {
    // Get the data-prototype explained earlier
    var prototype = $collectionHolder.data('prototype');

    // get the new index
    var index = $collectionHolder.data('index');

    var newForm = prototype;
    // You need this only if you didn't set 'label' => false in your tags field in TaskType
    // Replace '__name__label__' in the prototype's HTML to
    // instead be a number based on how many items we have
    // newForm = newForm.replace(/__name__label__/g, index);

    // Replace '__name__' in the prototype's HTML to
    // instead be a number based on how many items we have
    newForm = newForm.replace(/__name__/g, index);

    // increase the index with one for the next item
    $collectionHolder.data('index', index + 1);

    // Display the form in the page in an li, before the "Add a tag" link li
    var $newFormLi = $('<li></li>').append(newForm);
    $newFormLi.append('<button class="border-btn remove-tag" type="button" data-duplicate-remove="demo">Elimina</button>');

    $newLinkLi.before($newFormLi);
    //RIMUOVO CAMPI
        $('.remove-tag').click(function(e) {
        e.preventDefault();
        
        $(this).parent().remove();
        
        return false;
    });
    
}



